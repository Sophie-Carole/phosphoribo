#!/usr/bin/env python
# coding: utf-8

# In[10]:


#!/usr/bin/env python
# coding: utf-8

### Importation des packages
import requests 
from pprint import pprint
import re #à utiliser dans extrac et à remplacer par beautifulsoup par la suite
import sys
import pandas as pd # Pour stocker/ manipuler/ lire des données grace à des dataframe
from bs4 import BeautifulSoup # Pour l'analyse des textes/ fichiers HTML et XML 
import time 
from Bio import SeqIO, AlignIO # Pour la lecture & alignement des séquences
#import os, ssl # ??
#from Bio.Align.applications import ClustalOmegaCommandline
import codecs #Pour sauvegarder tous les nouveaux fichiers en UTF8 = encodage 
import csv
import urllib.parse
import urllib.request
import map_id

#truc

### Prediction des sites de phosphorylation avec SCANSITE
"""
SCANSITE: rechercher des motifs susceptibles d'être phosphorylés par des protéines kinases spécifiques 
dans les protéines données
"""           
## extrac permet d'extraire les données: motif, score, percentile .. correspondant à chaque protéine à partir du texte_résultat renvoyé par scansite
def extrac(texte):     
    motif=''
    site=''
    sequence=''
    score=''
    percentile=''

    motif_r= re.compile("<motifShortName>(.+)</motifShortName>")
    score_r= re.compile("<score>(.+)</score>")
    percentile_r= re.compile("<percentile>(.+)</percentile>")
    site_r= re.compile("<site>(.+)</site>")
    sequence_r= re.compile("<siteSequence>(.+)</siteSequence>")

    motif_match= re.search(motif_r, texte)
    score_match=re.search(score_r, texte)
    percentile_match= re.search(percentile_r, texte)
    site_match= re.search(site_r, texte)
    sequence_match= re.search(sequence_r, texte)
    
    lines = texte.split("\n")
    for line in lines:
        if motif_match is not None:
            motif=motif_match.group(1)          
        if score_match is not None:
            score=score_match.group(1)
        if percentile_match is not None:
            percentile=percentile_match.group(1)
        if site_match is not None:
            site=site_match.group(1)
        if sequence_match is not None:
            sequence=sequence_match.group(1)
    return(site, motif, score, percentile, sequence)


## La fonction milieu permet de lire le contenu du texte .html renvoyé par scansite 
def milieu(texte):
    lines = texte.split("\n")
    lignes= ''
    s=''; m=''; sc=''; per=''; seq=''; site=[]; motif=[]; score=[]; percentile=[]; sequence=[]
    milieu= False
    for line in lines:
        if line.startswith("<predictedSite>"):
            milieu= True
        elif line.startswith(r"</predictedSite>"):
            milieu = False
            s, m, sc, per, seq =extrac(lignes)
            site.append(s)
            motif.append(m)
            score.append(sc)
            percentile.append(per)
            sequence.append(seq)
            lignes= ''  # ne doit pas etre une liste mais une chaine de caractères
        elif milieu == True:
            lignes= lignes+'\n'+line
    return(site, motif, score, percentile, sequence)


# Request sur l'ensemble des ID_Entry_names:
def scan_site(Entry_id, stringence, kinase):
    server="https://scansite4.mit.edu/webservice/proteinscan"
    searched="/identifier="+str(Entry_id)+"/dsshortname=swissprot/motifclass=MAMMALIAN/motifshortnames="+str(kinase)+"/stringency="+str(stringence)+ "/referenceproteome=Vertebrata"
    m=requests.get(server+searched, verify=False) #verify= False pour résoudre l'erreur de SSL de ScanSite
    return(m.text)

## NB: La partie SCANSITE est à réecrire avec BeautifulSoup: Houyem


"""
PSP: Partie PhosphoSitePlus
"""
"""
Fichier: "Phosphorylation_site_dataset"
Pour une kinase donnée, la fonction cherche les sites qui ont été vus comme phosphorylés (+/- 1position) et
rapporte les données associées: LT_LIT,MS_LIT,MS_CST, etc...
P.S: ca affiche les données seulement (pas de dictionnaire/ pas de fichier de sortie !)= juste pour tester
"""
def psp_pd(fichier_pd, fichier_sc): #fichier_pd: fichier phosphorylation_dataset // fichier_sc: fichier de sortie de remp_sc
    # Ouvrir fichiers:
    df = pd.read_csv(fichier_pd, sep=';')
    # cols = [0, 4, 6, 9, 10, 11] # Dans le cas ou on veut sélectionnet les colonnes à lire 
    # df = pd.read_excel('C:/Users/touka/Desktop/Cours_M1_S2/Ptut/PTUT_1804/AATF_test.xlsx', sheet_names='Feuil1', usecols=cols)
    dfcomp= pd.read_csv(fichier_sc, sep=';') #fichier de sortie scansite pour la comparaison

    for tup in dfcomp.itertuples():
        for namedTuple in df.itertuples():
    #         print(namedTuple) # ok
            if namedTuple[7]== 'human':
                #if namedTuple[2]== ID_uniprot #Une autre vérification possible

                site_phosph = namedTuple[5].replace("-p","") #ResiduPhosphorylé dans Phosphorylated_dataset
                lettre1= site_phosph[0]
                num= int(''.join([str(i) for i in site_phosph if i.isdigit()])) #Pour extraire la position du résdu phosphorylé
    #             print(num)

                site_com= tup[3] #ResiduPhosphorylé dans Scansite pour meme prot
                lettre2= site_com[0]
                numcomp= int(''.join([str(i) for i in site_com if i.isdigit()]))
    #             print(numcomp)

                if (lettre1 == lettre2) and (num == numcomp or num==numcomp-1 or num==numcomp+1 ): #Tolérance d'erreur = 1 position
                    LT_LIT= namedTuple[11]
                    MS_LIT= namedTuple[12]
                    MS_CST= namedTuple[13]
                    print(site_phosph,site_com,LT_LIT,MS_LIT,MS_CST,sep=" -- ")

                # S477 et S478 / Y171 et Y170 / T146 et T147/ ..

                
"""
Cette partie est absolument a améliorer:

*permet de générer les Uniprot_ID des protéines phosphorylés présentes dans "Kinase_substrate_Dataset"/ "Disease_Associated_Dataset"
(On peut trouver des noms de sites qui se répétent mais qui n'appartiennent pas aux memes gènes, par exemple: 
on trouve 2 fois S169: meme position a chaque fois mais le gène_sub est différent :
et donc on doit comparer a chaque fois le site et le gène et pour comparer les gènes on doit avoir les meme identifiants !!))

* la fonction genère un nouveau fichier !
"""
def conv_psp(input, col, code, out): 
    with codecs.open(out, "w", 'utf-8') as output:
        writer = csv.writer(output, delimiter = '\t', lineterminator = '\n')
        if code ==1:
            entetes = ['GENE','KINASE','KIN_ACC_ID','KIN_ORGANISM','SUBSTRATE','SUB_GENE_ID','SUB_ACC_ID','SUB_GENE','SUB_ORGANISM','SUB_MOD_RSD','SITE_GRP_ID','SITE_+/-7_AA','DOMAIN','IN_VIVO_RXN','IN_VITRO_RXN','CST_CAT','UNIPROT_ID']
        elif code ==2:
             entetes = ['DISEASE','ALTERATION','GENE','PROTEIN','ACC_ID','UNIPROT_ID','GENE_ID','HU_CHR_LOC','MW_kD','ORGANISM','SITE_GRP_ID','MOD_RSD','DOMAIN','SITE_+/-7_AA','PMIDs','LT_LIT','MS_LIT','MS_CST','CST_CAT#','NOTES']
        
        ligneEntete = "\t".join(entetes) + "\n"
        output.write(ligneEntete)
    ######################################################################################
        df = pd.read_csv(input, sep=';', encoding= 'utf8') #Pardéfaut, ça suppose qu'il y a un header // faut ajouter sep a chaque fois pour les erreurs !
        queryl=''     
        for tup in df.itertuples():
            
            ## if tup[9] == 'human':
            queryl = str(tup[col]) # Lire un entry_name à la fois
            
            url = 'https://www.uniprot.org/uploadlists/'
            params1 = {
            'from': 'ACC+ID',
            'to': 'ID',
            'format': 'tab',
            'query': queryl
            }
            
            data1 = urllib.parse.urlencode(params1)
            data1 = data1.encode('utf-8')
            req1 = urllib.request.Request(url, data1)
            
            try:
                with urllib.request.urlopen(req1, timeout=60) as f1:
                    response1 = f1.readlines()
#                     print(response1)
            except Exception as e :  # Pour gérer l'erreur HTTPError !!
                print(str(e)) 
    ######################################################################################
            lis1=[] # Cas ou il y a plus qu'un Uniprot_ID ??
            for m in response1:
                ligne1= m.decode('utf-8')
                mots1= ligne1.split()
                if mots1[1]== "To":
                    continue
                else:
                    lis1.append(mots1[1])
                
            if code ==1:      
#                 d = pd.DataFrame(tup)
#                 m= [d.to_string(index = False) + "\t".join(lis1)]
                m= [str(tup[1]),str(tup[2]),str(tup[3]),str(tup[4]),str(tup[5]),str(tup[6]),str(tup[7]),str(tup[8]),str(tup[9]),str(tup[10]),str(tup[11]),str(tup[12]),str(tup[13]),str(tup[14]),str(tup[15]),str(tup[16]), ",".join(lis1)]
            elif code== 2:
                m= [str(tup[1]),str(tup[2]),str(tup[3]),str(tup[4]),str(tup[5]),",".join(lis1),str(tup[6]),str(tup[7]),str(tup[8]),str(tup[9]),str(tup[10]),str(tup[11]),str(tup[12]),str(tup[13]),str(tup[14]),str(tup[15]),str(tup[16]),str(tup[17]),str(tup[18]),str(tup[19])]   
#             print(m)
            writer.writerow(m)
        output.close()
# Kinase_substrate_dataset: 15263 lignes human !! 6H pour traiter 6000 lignes a peu près !!


"""
Fichier: "Kinase_substrate_Dataset"
*permet de récuperer si le site a été vu phosphorylé in vivo ou in vitro et par quelle kinase (nom de la kinase + Acc_ID)
P.S: Chaque fonction genère un nouveau fichier !!
"""
        
def psp_ksd(f_sc, f_ksd, f_sortie): #f_ksd: c le fichier de sortie de conv_ksd !!
    with codecs.open(f_sortie, 'a','utf-8') as fout:
        writer = csv.writer(fout,  delimiter = '\t', lineterminator='\n')
        entetes = ['Entry_name','UniProt_ID', 'Site_SC', 'Kinase_SC','Motif', 'Percentile', 'Score', 'kinase_PSP', 'Kinase_ACC_ID', 'In_VIVO', 'In_VITRO']
        ligneEntete = "\t".join(entetes) + "\n"
        fout.write(ligneEntete)    

        # Ouvrir fichiers:
        df1= pd.read_csv(f_ksd, sep=';') #f_ksd avec id Uniprot
        df2= pd.read_csv(f_sc, sep=';') #f_sc

        for tup in df2.itertuples():
            ligne=[]
            for namedTuple in df1.itertuples():
                if namedTuple[4]== 'human':
                    if tup[2]== namedTuple[17]: # Verifier que c le meme gène
                        site_phosph = namedTuple[10] #ResiduPhosphorylé
                        lettre1= site_phosph[0]
                        num= int(''.join([str(i) for i in site_phosph if i.isdigit()]))
                        
                        site_com= tup[3] #ResiduPhosphorylé dans Scansite pour meme prot
                        lettre2= site_com[0]
                        numcomp= int(''.join([str(i) for i in site_com if i.isdigit()]))
                        if (lettre1 == lettre2) and (num == numcomp or num==numcomp-1 or num==numcomp+1 ):
                            ligne= [tup[1],tup[2],tup[3],tup[4],tup[5],tup[6],tup[7],namedTuple[2],namedTuple[3],namedTuple[14],namedTuple[15]]
#                             print(ligne)
                            writer.writerow(ligne)
            fout.close()

"""
Fichier: "Disease_Associated_Sites"
Pour une kinase donnée, la fonction permet de chercher les maladies associées aux sites de phosphorylation prédits par ScanSite 
& crée un fichier avec les données croisées de l'analyse des 2 tables / fichiers
"""
def psp_das(fichier_sc, fichier_das, fichier_sortie):
    with codecs.open(fichier_sortie, 'a','utf-8') as fout:
        writer = csv.writer(fout,  delimiter = '\t', lineterminator='\n')
        entetes = ['Entry_name','UniProt_ID', 'Site_SC', 'Kinase_SC', 'Motif', 'Percentile', 'Score',"site_PSP", "Alteration", "Maladie", "Proteine-domaine", "lt_lit", "ms_lit", "ms_cst", "Notes"]
        # Headers:
        ligneEntete = "\t".join(entetes) + "\n"
        fout.write(ligneEntete)    

        dfa = pd.read_csv(fichier_das, sep=';', encoding= 'utf8')
        dfb= pd.read_csv(fichier_sc, sep=';', encoding= 'utf8')

        for linea in dfa.itertuples():
            #Iere verification:
            if linea[10]== 'human': # On ne COMMENCE PAS de 0 mais de 1
                ligne=[]
                for lineb in dfb.itertuples():
                    # IIeme verification:
                    if linea[6]== lineb[2]: # Si c'est le meme gène / prot
                        if (linea[12]).endswith('-p'): #verifier qu'il s'agit d'une phosphorylation (et pas méthylation/ acétylation..)
                            site_phosph = linea[12].replace("-p","")
                            lettre1= site_phosph[0]
                            num= int(''.join([str(i) for i in site_phosph if i.isdigit()]))
                            site_sc= lineb[3]
                            lettre2= site_sc[0]
                            numsc= int(''.join([str(i) for i in site_sc if i.isdigit()]))

                            if (lettre1 == lettre2) and (num == numsc or num==numsc-1 or num==numsc+1):
                                alt= linea[2]
                                dis= linea[1]
                                protdom= str(linea[4])+ " - "+ str(linea[13])
                                lt_lit= linea[16]
                                ms_lit= linea[17]
                                ms_cst= linea[18]
                                notes= linea[20]
                                ligne= [lineb[1],lineb[2],lineb[3],lineb[4],lineb[5],lineb[6], lineb[7], site_phosph, alt, dis, protdom, lt_lit, ms_lit, ms_cst, notes]
                                print(ligne)
                                writer.writerow(ligne)
        fout.close()


### Recherche des Orthologues 
"""
orthologues:
parsing de la db ensembl
renvoie une liste de protéines orthologues 
"""
## Recherche de proteines orthologues à une prot donnée (ID) dans une espèce donnée (specie)
def orthol(ID, specie):
    orthologues=[]
    server = "https://rest.ensembl.org"
    ext = "/homology/id/"+ID+"?type=orthologues;target_species="+specie;"type=orthologues"
    r=requests.get(server+ext, headers={ "Content-Type" : "text/x-orthoxml+xml"})
    if not r.ok:
        r.raise_for_status()
        sys.exit()
    doc=r.text
    #print(doc)
    soup=BeautifulSoup(doc, 'html.parser') 
    #print(soup.prettify())
    for el in soup.find_all('gene'):
        orthologues.append(el.get('protid'))
    return orthologues

## Récupération de la séquence proteique (orthologue)
def recup_sequence(ID):
    server = "https://rest.ensembl.org"
    ext = "/sequence/id/"+ID+"?type=protein"
    r = requests.get(server+ext, headers={ "Content-Type" : "text/x-fasta"}, params=[('q','requests+language:python')])
 
    if not r.ok: ## ?? Explanation please ?? 
        r.raise_for_status()
        sys.exit()
    
    fasta=r.text
#     print(fasta.split('\n',1)[1]) #pour ne recuperer que la sequence de lettres sans le '>ENSxxx\n': résoudre le blem de '\n' dans la création du fichier .csv
    return(fasta.split('\n',1)[1])


"""
Création de 2 dictionnaires: le premier avec les résultats ScanSite + PSP, le deuxième avec les résultats de recherche d'orthologues
==> 2 fichiers_résultats pour chaque kinase étudiée
"""  
def creation_dico(fichier, stringence, kinase): #fichier: pour fichier output issu de conv_id
    d_b={} #dictionnaire ScanSite
    d_ortho= {} # dictionnaire orthologues
    
    species=("danio_rerio","mus_musculus","rattus_norvegicus","caenorhabditis_elegans","gallus_gallus","drosophila_melanogaster")
    
#     df=pd.read_csv(fichier, sep='\t', header=None, names =['Entry_name','IDuniprot','IDensembl']) #dataframe containing the tabulated file with the identifiers    
    df=pd.read_csv(fichier, sep=';', names =['Entry_name','IDuniprot','IDensembl'], header = 0) # header = 0 pour fichier avec entete // faut faire attention au séparateur
    # Pour df, je voulais utiliser le fichier généré par conv_id mais il faut résoudre le problème des Ensembl_ID multiples !!!

    for row in df.itertuples(): #allows iteration on each row of the dataframe and thus selecting any element I want on the row (here 1= uniprot ID and 2=Ensembl ID (0=index))
        Entry_name= row[1]
        identifiant=row[2]
        idEnsembl=row[3]
        
        text= str(scan_site(identifiant, stringence, kinase).replace("><", ">\n<")) # Appel scan_site
        site, motif, score, percentile, sequence= milieu(text)
        d_b[Entry_name]={'uniprotID':identifiant}
        d_b[Entry_name]['sites']={}
        
        d_ortho[Entry_name]={'uniprotID':identifiant}
        d_ortho[Entry_name]['ensemblID']={}
        d_ortho[Entry_name]['ensemblID']=idEnsembl
        d_ortho[Entry_name]['orthologues']={}
        
        while site:
            sites=site.pop()
            d_b[Entry_name]['sites'][sites]={}
            data=d_b[Entry_name]['sites'][sites]
            data['kinase']=motif.pop()
            data['score']=score.pop()
            data['percentile']=percentile.pop()
            data['motif']=sequence.pop()
        
        for specie in species:
            if len(d_b[Entry_name]['sites']) >0 : # do the ortholog stuff only if there is at least one site predicted for this protein(=/= 'sites':{})
                orthologs=orthol(idEnsembl, specie)
                
                ortho= d_ortho[Entry_name]['orthologues']
                ortho[specie]={}
                
                if len(orthologs) == 0 :
                    ID_HumanProt= float('NaN')
                    ID_orthol=float('NaN')
                    seq_fasta= float('Nan')
                    
                else:
                    ID_HumanProt=orthologs.pop(0)
                    if len (orthologs) >= 1 : #c'etait >2 : dans tous les cas créer une liste => ca facilite la création du fichier .csv
                        i=1
                        while len(orthologs) !=0 :
                            ID_orthol=orthologs.pop()
                            seq_fasta=recup_sequence(ID_orthol)
                            
                            ortho[specie]["Ortho"+str(i)]={'ID':{}, 'seq':{}}
                            ortho[specie]["Ortho"+str(i)]['ID']=ID_orthol
                            ortho[specie]["Ortho"+str(i)]['seq']=seq_fasta
                            i+=1
    return d_b, d_ortho


"""
Création du fichier .csv avec le dictionnaire ScanSite
"""
## Création fichier .CSV à partir du dictionnaire rempli:
def remp_sc(FILENAME, data): #FILENAME: nom du fichier de sortie !! / data dictionnaire scansite
    with codecs.open(FILENAME, "a", 'utf-8') as output:
        writer = csv.writer(output, delimiter = '\t', lineterminator = '\n')
        entetes = ['Entry_name','UniProt_ID', 'Site', 'Kinase', 'Motif', 'Percentile', 'Score']
        ligneEntete = "\t".join(entetes) + "\n"
        output.write(ligneEntete)

        for key in data.keys():
#         print(key) # entry_name
            unip= data[key]['uniprotID']
            for site in data[key]['sites']:
                print(site)
                a= data[key]['sites'][site]['kinase']
                print(a)
                b= data[key]['sites'][site]['motif']
                print(b)
                c= data[key]['sites'][site]['percentile']
                print(c)
                d= data[key]['sites'][site]['score']
                print(d)
                writer.writerow([key, unip, site, a, b, c, d])        
    output.close()


## Creation fichier .CSV avec les résultats de recherche d'orthologie:
"""
Création du fichier .csv avec le dictionnaire Orthologues
"""
def remp_ortho(FILENAME, data2):
    with codecs.open(FILENAME, "w", 'utf-8') as output:
        writer = csv.writer(output, delimiter = '\t', lineterminator = '\n')
        entetes = ['Entry_name', 'Uniprot_ID', 'Ensembl_ID', 'Espece', 'Num_Ortho', 'ID_Orthologue', 'Seq_Orthologue']
        ligneEntete = "\t".join(entetes) + "\n"
        output.write(ligneEntete)    

        for key in data2.keys():
                print(key)
                unip= data2[key]['uniprotID']
                Ens_id= data2[key]['ensemblID'] # Cas où il n'y a qu'un seul EnsemblId pour chaque prot
                print(Ens_id)

                for esport in data2[key]['orthologues']: # Pour tous les orthologues: un ou plus
                    print(esport) #donne l'espèce d'ortho
                    
                    i=0 #Pour compter nombre d'orthologues
                    for ortho in data2[key]['orthologues'][esport]:
                        ID_ortho = data2[key]['orthologues'][esport][ortho]['ID']
                        print(data2[key]['orthologues'][esport][ortho]['ID'])
                        Seq_ortho= data2[key]['orthologues'][esport][ortho]['seq'].replace("\n", "") # les "\n" dans la séquence posent un pblem lors de la création du fichier de sortie (la séquence sera ércite sur plusieurs lignes !!)
                        print(Seq_ortho)
                        i +=1

                        writer.writerow([key, unip, Ens_id, esport,i ,ID_ortho, Seq_ortho])    
        output.close()

###################################################    MAIN    ######################################################

### PROGRAMME PRINCIPAL: ça pourrait etre mis dans un fichier séparé (ca permet à l'utilisateur de rentrer les param souhaités et de ne pas toucher aux autres fonctions !)
if __name__ == "__main__":
      
    # ~ start0=time.time()
    # ~ conv_id("IDs.csv", "Liste_id.csv") # "IDs.csv": input= fichier d'entrée avec Entry_names // "Liste_id.csv": fichier de sortie
    # ~ print("Tiiiiiiiiiiiiime\n")
    # ~ print(time.time()-start0) #1403.2336165904999 ~ 23 min
        
    ## Boucle sur les =/= kinases: ##
    start1=time.time()
    data_sc, data_ortho =creation_dico("identifiants_test.tsv",'Low', "Akt_Kin")
#     pprint(data_sc)
#     pprint(data_ortho)
    print("Tiiiiiiiiiiiiime\n")
    print(time.time()-start1) #4101.9531686306 ~1h13min
    
    start2=time.time()
    remp_sc("Liste_akt.csv", data_sc) #0.37495970726013184
    remp_ortho("Liste_ortho.csv", data_ortho) #0.4169731140136719
    print("Tiiiiiiiiiiiiime\n") 
    print(time.time()-start2)

    start3=time.time()
    psp_pd("Phosphorylation_site_dataset.csv", "Liste_akt.csv") #f1:fichier Phosphorylation_dataset télechargé depuis PSP / f2: fichier_résultat de ScanSite
    print("Tiiiiiiiiiiiiime\n") 
    print(time.time()-start3) #1509.175321340561 ~ 25 min
    
    start4=time.time()
    conv_ksd("Kinase_Substrate_Dataset.csv", 7, 1, "ksd_akt_bis.csv") #fichier d'entrée et fichier de sortie
    psp_ksd("ksd_bis.csv", "Kinase_Substrate_Dataset.csv", 'Liste_Akt_Final.csv')
    print("Tiiiiiiiiiiiiime\n") 
    print(time.time()-start4) # ~15Heures
    
    start5=time.time()
    conv_psp("Disease_associated_sites.csv", 5, 2, "DAS_bis.csv") # 4291.585020542145 ~1h 11 min
    psp_das("Liste_akt.csv","DAS_bis.csv", "Liste_Disease_Akt.csv") #4.988784074783325
    print("Tiiiiiiiiiiiiime\n")
    print(time.time()-start5)
 
    
# Temps total= 


# In[ ]:




