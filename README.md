# Projet

Sur un ensemble de protéines (facteurs d'assemblage et de maturation des ribosomes), prédire les sites phosphorylés par les kinases choisies puis vérifier la vraisemblance de cette prédiction.

### Prédiction 

- Scansite

prog/scansite.py -> utilise API scansite

### Calcul de scores de vraisemblances

- Vraisemblance in vivo (in vitro aussi?)

prog/psp.py -> utilise les bases de données de phosphoprotéomes (phosphositeplus)

- Conservation du site reconnu par les kinases d'intérêt au sein des protéines orthologues chez des organismes modèles (pour l'instant seulement vertébrés)

prop/orthol.py -> API Ensembl 


----

## Version longue

### Introduction

Ribosomes are complexes consisting of two subunits, one large and one small, that catalyse protein synthesis. Thus ribosome biogenesis is a major process in cell life. Many factors are involved in this process: RNA polymerases, small RNAs, proteins and snoRNP. Among the proteins involved, more than 300 co-factors (called assembly and maturation factors) participate in the maturation of the ribosomes precursors. An increase in the ribosome synthesis results in an increase of the protein synthesis. Studies have shown that the signaling pathways : PI3K/AKT/mTOR and RAS/MAPK are responsible for regulating proliferation and growth cells, controlling therefore the synthesis of ribosomes.
Past studies allowed the identification of about 300 assembly and maturation factors in human cells. Data suggest that some of these factors are target for phosphorylation within the signaling pathways : AKT/mTOR and RAS/MAPK. 

This project aims to computationally predict phosphorylation sites under the control of kinases belonging to AKT/mTOR and Ras/MAPK pathways and to classify them by computing a degree of conservation and verifying their existence in vivo. 
A list of sites phosphorylated by the 3 AGC kinases AKT, RSK and S6K as well as the kinase ERK has already been manually gathered, and therefore must involve many mistakes. Other issues that we can raise is that this was a time-consuming, redundant work demanding a sustained focus, also the decision made to determine whether a site was accurately predicted or not was arbitrary in some instances. The computation method would overcome these issues. In addition it would allow the prediction to be updated, the dataset of proteins to be augmented easily and the parameters set in order to make the prediction to be editable. 

This project can be split in several tasks:
+ computationally searching for patterns recognized by the kinases of interest
+ testing the pattern’s degree of conservation throughout different species
+ browsing studies that have shown that the site has been detected as phosphorylated in vivo

### Methods

In order to conduct this study, we will use : 

A list of assembly factors identifiers (UniProt ID) of Humans

A list of patterns recognized by the kinases of interest 

A list of organisms that will be used in our research to determine the conservation degree of phosphorylation sites among species.
Also, two lists of positives and negatives will be provided in order to build the classification model and test its robustness.

The different parts of the script code will be developed using Python. This programming language was chosen for its power and ease of use. Python language also offers several features thanks to its modules and packages which will help us modularize our code, and make it much easier to deal with.

- Prediction of phosphorylation sites

The first step of this study consists in computationally searching for patterns recognized by the kinases of interest on different human proteins (assembly factors) using a program called Scansite. The expected result will be a list of sites potentially phosphorylated with an associated score which will be taken into consideration for the subsequent classification of these potential sites.

- Assessment of the site’s pattern’s degree of conservation

A strong proof of evidence for the existence of these sites is to test the pattern’s degree of conservation throughout different species. In that respect, we will search for the factors’ homologs/orthologs in order to do a multiple sequence alignment which will allow us to compute a conservation score for each pattern. This step will be carried out using data from the Ensembl genome database which gathers genomic information related to different species such as vertebrates and other model organisms.

- Assessment of relevance in Vivo

Another way to validate the existence of a predicted site is to browse studies which document the site as phosphorylated. PhosphositePlus which gathers databases of phosphoproteomes will be our main source. The result of this task will be a score associated with each site expressing the amount and the kind of studies in which the site has been seen phosphorylated. 

- Classification algorithm

Eventually we will implement a classification algorithm that will take into account all the scores previously computed in order to decide whether or not the predicted site is potentially a phosphorylated site by one of the studied kinases of interest. By the end of this classification, we expect to obtain a confidence value for each sites. 

### Results

To be continued ...
