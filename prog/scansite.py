### Importation des packages
import requests 
from pprint import pprint
import re #à utiliser dans extrac et à remplacer par beautifulsoup par la suite
import sys
import pandas as pd # Pour stocker/ manipuler/ lire des données grace à des dataframe
from bs4 import BeautifulSoup # Pour l'analyse des textes/ fichiers HTML et XML 
import time 
from Bio import SeqIO, AlignIO # Pour la lecture & alignement des séquences
#import os, ssl # ??
#from Bio.Align.applications import ClustalOmegaCommandline
import codecs #Pour sauvegarder tous les nouveaux fichiers en UTF8 = encodage 
import csv
import urllib.parse
import urllib.request
import json 



### Prediction des sites de phosphorylation avec SCANSITE
"""
SCANSITE: rechercher des motifs susceptibles d'être phosphorylés par des protéines kinases spécifiques 
dans les protéines données
"""           
# Request sur l'ensemble des ID_Entry_names:
def scan_site(Entry_name, kinase, stringence='Low'):    
    server="https://scansite4.mit.edu/webservice/proteinscan"
    searched="/identifier="+Entry_name+"/dsshortname=swissprot/motifclass=MAMMALIAN/motifshortnames="+str(kinase)+"/stringency="+ stringence + "/referenceproteome=Vertebrata"  ## mettre motifshortnames en argument à la fonction également
    m=requests.get(server+searched)
    return(m.text)
## renvoie tout le texte xml de scansite
    
## extrac permet d'extraire les données: motif, score, percentile .. correspondant à chaque protéine à partir du texte_résultat renvoyé par scansite
def extrac(texte): # texte= on donne scansite(Entry_name, kinase, stringence)
    motif=[]
    site=[]
    sequence=[]
    score=[]
    percentile=[]
    #texte= scan_site("AATF_HUMAN", "Akt_kin", "Low")
    #print(texte)
    soup = BeautifulSoup(texte, "xml")
    #print(soup.prettify()) #affichage avec retour en ligne

    for mot in soup.find_all('motifName'):
        #print(mot.get_text())
        motif.append(mot.get_text())
    for per in soup.find_all('percentile'):
        #print(per.get_text())
        percentile.append(per.get_text())
    for sc in soup.find_all('score'):
        #print(sc.get_text())
        score.append(sc.get_text())   
    for sit in soup.find_all('site'):
        #print(sit.get_text())
        site.append(sit.get_text())    
    for seq in soup.find_all('sequence'):
        #print(seq.get_text())
        sequence.append(seq.get_text())    


    return(site, motif, score, percentile, sequence)


    
"""

Vérifie la proximité de deux sites selon une distance epsilon

"""    

def ensemble(site1, site2, eps):
	site1=site1.replace("S","")
	site1=site1.replace("T","")
	site1=site1.replace("P","")
	site1=int(site1)
	site2=site2.replace("S","")
	site2=site2.replace("T","")
	site2=site2.replace("P","")
	site2=int(site2)
	if abs(site1 - site2) > eps:
		return False
	return True


"""

Cas particulier pour Erk qui combine trois sites reconnus par la kinase erk 
Prend en argument toutes structures de données relatives à erk et combine les résultats dans une seule structure 

"""

def erk_case (ERK_D_Dom_dico_final, ERK1_Binding_dico_f, ERK1_kin_dico_f, ERK_D_Dom_dico_utile, ERK1_Binding_dico_u, ERK1_kin_dico_u):
	dfinal=ERK_D_Dom_dico_final
	df2=ERK1_Binding_dico_f
	df3=ERK1_kin_dico_f
	d1=ERK_D_Dom_dico_utile
	d2=ERK1_Binding_dico_u
	d3=ERK1_kin_dico_u
	
	for key1, values1 in d1.items():
		for key2, values2 in d2.items():
			for key3, values3 in d3.items():
				if key2 == key3:
					supprimer=[]
					for site2 in values2['sites'].keys():
						for site3 in values3['sites'].keys():
							if ensemble(site2, site3, 100) and key1 != key2:

								dfinal.update({key2+"_"+site2:{'kinase':'', 'site':'','score':'','percentile':'','score_conservation':'0','score_invivo':'0', 'classif':''}})
								dfinal[key2+"_"+site2]['site']=site2		
								dfinal[key2+"_"+site2]['score']=values2['sites'][site2]['score']
								dfinal[key2+"_"+site2]['percentile']=values2['sites'][site2]['percentile']
								dfinal[key2+"_"+site2]['kinase']=values2['sites'][site2]['kinase']
								
								dfinal.update({key2+"_"+site3:{'kinase':'', 'site':'','score':'','percentile':'','score_conservation':'0','score_invivo':'0', 'classif':''}})
								dfinal[key2+"_"+site3]['site']=site3		
								dfinal[key2+"_"+site3]['score']=values3['sites'][site3]['score']
								dfinal[key2+"_"+site3]['percentile']=values3['sites'][site3]['percentile']
								dfinal[key2+"_"+site3]['kinase']=values3['sites'][site3]['kinase']
								
								d1[key2]['sites'].update(values2['sites'])
								d1[key2]['sites'].update(values3['sites'])

								
								for key in d1[key2]['sites'].keys():
									if key != site2 and key != site3 and d1[key2]['sites'][key]['kinase'] != "ErkDD":
										supprimer.append(key)
								
								supprimer= list(set(supprimer))
								
								for element in supprimer:									
									del d1[key2]['sites'][element]
								

	return dfinal, d1
					

"""

Fonction de création et initialisation des structures de données qui vont stocker les informations après chaque module 
- un dictionnaire qui contient les résultats finaux
- un dictionnaire avec les infomations intermédiaires utiles 

"""

def init_dico(fichier, kinase, stringence):
    dictionnaire_final={} 
    dictionnaire_utile={}

    df=pd.read_csv(fichier, sep='\t', header=None, names =['Entry_name','IDuniprot','IDensembl']) 
    # ~ df=pd.read_csv(fichier, sep=';', names =['Entry_name','IDuniprot','IDensembl'], header = 0) # header = 0 pour fichier avec entete // faut faire attention au séparateur

    for row in df.itertuples(): 
        Entry_name= row[2]
        identifiant=row[1]        
        text= str(scan_site(identifiant, kinase, stringence).replace("><", ">\n<")) # Appel scan_site
        site, motif, score, percentile, sequence= milieu(text)
        dictionnaire_utile[identifiant]={'uniprotID':Entry_name}
        dictionnaire_utile[identifiant]['sites']={}
        
        while site:
            sites=site.pop()
            dictionnaire_final[identifiant+"_"+sites]={'kinase':'', 'site':'','score':'','percentile':'','score_conservation':'','score_invivo':''}
            df=dictionnaire_final[identifiant+"_"+sites]
            df['site']=sites
            dictionnaire_utile[identifiant]['sites'][sites]={'kinase':'','score':'','percentile':'','motif':'','LT_LIT':'','MS_LIT':'', 'MS_CST':'', 'in_vitro':'0','in_vivo':'0'}
            data=dictionnaire_utile[identifiant]['sites'][sites]
            data['LT_LIT']=0.0
            data['MS_LIT']=0.0
            data['MS_CST']=0.0
            kin=motif.pop()
            data['kinase']=kin
            df['kinase']=kin
            scores=score.pop()
            df['score']=scores
            data['score']=scores
            percentiles=percentile.pop()
            df['percentile']=percentiles
            data['percentile']=percentiles
            data['motif']=sequence.pop()
            df['kinase']=kin
    return dictionnaire_final, dictionnaire_utile       



if __name__ == "__main__":
	d1, d2 = init_dico('identifiants_test.tsv','ErkDD','Low')
	d3, d4 = init_dico('identifiants_test.tsv','Erk1_Bind','Low')
	d5, d6 = init_dico('identifiants_test.tsv','Erk1_Kin','Low')
	
	d7, d8= erk_case(d1,d2,d4,d6)
	
	# ~ with open('dfinal_akt_low.txt','w') as json_f:
		# ~ json.dump(d1, json_f)
	# ~ with open('dutile_akt_low.txt','w') as json_file:
		# ~ json.dump(d2, json_file)
	

	print("----")
	pprint(d7)
	

""" 
exemple de sortie pour dictionnaire_final:
{'Q14137_S279': {'percentile': '0.027863310430846475',
                 'score': '0.6643',
                 'site': 'S279'},
 'Q14137_S341': {'percentile': '0.0197671821866803',
                 'score': '0.6342',
                 'site': 'S341'},
 'Q14137_S420': {'percentile': '0.012587588319580685',
                 'score': '0.5968',
                 'site': 'S420'},
 'Q14137_S571': {'percentile': '0.039641893634309505',
                 'score': '0.6979',
                 'site': 'S571'},
 'Q14137_S573': {'percentile': '0.01465182401413347',
                 'score': '0.609',
                 'site': 'S573'},
 'Q14137_T277': {'percentile': '0.0034590024400051205',
                 'score': '0.5042',
                 'site': 'T277'},



pour dictionnaire_utile:
 'BRX1_HUMAN': {'sites': {'T207': {'kinase': 'Akt_Kin',
                                   'motif': 'PFVDHVFtFTILDNR',
                                   'percentile': '0.023889774730100587',
                                   'score': '0.6504'},
                          'T273': {'kinase': 'Akt_Kin',
                                   'motif': 'RRVIRSItAAKYREK',
                                   'percentile': '0.03804184932181668',
                                   'score': '0.694'},
                          'T35': {'kinase': 'Akt_Kin',
                                  'motif': 'PPAKRHAtAEEVEEE',
                                  'percentile': '0.023863303614643158',
                                  'score': '0.6503'}},
                'uniprotID': 'Q8TDN6'},
 'EHEAT1_HUMAN': {'sites': {'S1808': {'kinase': 'Akt_Kin',
                                      'motif': 'QANIRLTsLKKTLAT',
                                      'percentile': '0.04541441736508015',
                                      'score': '0.7107'},
                            'S731': {'kinase': 'Akt_Kin',
                                     'motif': 'PFAIRVFsLLQKKIK',
                                     'percentile': '0.023413294651866742',
                                     'score': '0.6486'},
                            'T1935': {'kinase': 'Akt_Kin',
                                      'motif': 'APKDRLLtFYNLADC',
                                      'percentile': '0.012719476428862694',
                                      'score': '0.5977'}},
                  'uniprotID': 'Q9H583'},

"""
