### Importation des packages
import requests 
from pprint import pprint
import re #à utiliser dans extrac et à remplacer par beautifulsoup par la suite
import sys
import pandas as pd # Pour stocker/ manipuler/ lire des données grace à des dataframe
from bs4 import BeautifulSoup # Pour l'analyse des textes/ fichiers HTML et XML 
import time 
from Bio import SeqIO, AlignIO # Pour la lecture & alignement des séquences
#import os, ssl # ??
#from Bio.Align.applications import ClustalOmegaCommandline
import codecs #Pour sauvegarder tous les nouveaux fichiers en UTF8 = encodage 
import csv
import urllib.parse
import urllib.request





"""
Conversion des identifiants 


Ce bout de code permet de prendre en entrée des identifiants 'Entry_name' ,
et de renvoyer les identifiants Uniprot et Ensembl correspondants
"""
def conv_id(input, FILENAME): #FILENAME cad fichier_output
    
    #Création d'un fichier .csv avec les IDs (on peut supprimer cette partie par la suite si on veut pas stocker les ids dans un fichier)
    with codecs.open(FILENAME, "w", 'utf-8') as output:
        writer = csv.writer(output, delimiter = '\t', lineterminator = '\n')
        entetes = ['Entry_ID','Entry_name', 'Ensembl_ID']
        ligneEntete = "\t".join(entetes) + "\n"
        output.write(ligneEntete)
    ######################################################################################
        df = pd.read_csv(input, sep=';') #Lecture des Entry_name
        queryl=''     
        for tup in df.itertuples():
            queryl = str(tup[3]) # Lire un entry_name à la fois
            #queryl += str(tup[1])+' ' # Pour remplir une liste avec tous les identifiants 

            url = 'https://www.uniprot.org/uploadlists/'
            params1 = {
            'from': 'ACC+ID',
            'to': 'ID',
            'format': 'tab',
            'query': queryl
            } # 1ère conversion des Enrty_name à Uniprot_IDs
            params2 = {
            'from': 'ACC+ID',
            'to': 'ENSEMBL_ID',
            'format': 'tab',
            'query': queryl
            } # 2ème conversion des Entry_name à Ensembl_IDs

            data1 = urllib.parse.urlencode(params1)
            data1 = data1.encode('utf-8')
            req1 = urllib.request.Request(url, data1)
            with urllib.request.urlopen(req1) as f1:
                response1 = f1.readlines()
    #             print(response1)

            data2 = urllib.parse.urlencode(params2)
            data2 = data2.encode('utf-8')
            req2 = urllib.request.Request(url, data2)
            with urllib.request.urlopen(req2) as f2:
                response2 = f2.readlines()
    #             print(response2)

    ######################################################################################
            lis1=[] # Cas ou il y a plus qu'un Uniprot_ID ??
            for m in response1:
                ligne1= m.decode('utf-8')
                mots1= ligne1.split()
                if mots1[1]== "To":
                    continue
                else:
                    lis1.append(mots1[1])
    #         print(lis1)

            lis2=[] # C possible d'avoir plusieurs Ensembl_id 
            for m in response2:
                ligne2= m.decode('utf-8')
                mots2= ligne2.split()
                if mots2[1]== "To":
                    continue
                else:
                    lis2.append(mots2[1])       
    #         print(lis2)
#             print([queryl,",".join(lis1),",".join(lis2)])
            writer.writerow([queryl,",".join(lis1),",".join(lis2)])
            #return [queryl,",".join(lis1),",".join(lis2)]
        output.close()
    ######################################################################################
