import pandas as pdf
from scansite import *
from psp import*
from pprint import pprint


def positifs(fichier_kinase_dataset, fichier_phosphoylation_dataset, kinase, liste_proteines): 
	df= pd.read_csv(fichier_kinase_dataset, sep='\t') 
	dfstudies = pd.read_csv(fichier_phosphoylation_dataset, sep = '\t')
	dfprot= pd.read_csv(liste_proteines, sep ='\t')
	équivalences_kinases={"Akt_kin" : ("Akt1", "Akt2", "Akt3"), "mTOR": ("mTOR"), "ErkDD": ("ERK1", "ERK2", "ERK3")}  #à compléter 
	for kin in équivalences_kinases[kinase]:
		for prot in dfprot.itertuples():
			for row in df.itertuples():
				if row[4]=='human' and row[2]==kin: #and row[14]=='X':
					identifiant_prot= row[7]
					if identifiant_prot == prot[1]:
						positif=identifiant_prot
						site_phospho=row[10]	
						for ligne in dfstudies.itertuples():
							if ligne[7]=='human' and ligne[3]==identifiant_prot :
								site_phosph = ligne[5].replace("-p","")
								if site_phosph == site_phospho:
									LT=str(ligne[11])
									MS=str(ligne[12])
									
									print(positif, site_phospho, row[1], row[2], "invitro: "+row[15], "invivo: "+row[14], "LT: ",ligne[11], "MS: ",ligne[12], sep="\t" )
									



def fetch_seqs(fichier_kinase_dataset, fichier_phosphoylation_dataset, kinase, liste_proteines): 
	df= pd.read_csv(fichier_kinase_dataset, sep='\t') 
	équivalences_kinases={"Akt_kin" : ("Akt1", "Akt2", "Akt3"), "mTOR": ("mTOR"), "ErkDD": ("ERK1", "ERK2", "ERK3")}  #à compléter 
	i=1
	for kin in équivalences_kinases[kinase]:
		for row in df.itertuples():
			if row[4]=='human' and row[2]==kin: #and row[14]=='X':
				identifiant_prot= row[7]
				seq=row[12]
				print('>',i)		
				print(row[12], sep="\t" )
				i+=1


def comparaison_pos(file_pos, dico1, dico2):
	fichier=pd.read_csv(file_pos, sep='\t')
	for row in fichier.itertuples():
		for ident,val in dico1.items():
			if row[1]==ident:
				#print(row[1])
				for site in dico1[ident]['sites'].keys():
					if site==row[2]:
						dico2[ident+"_"+site]['classif']=1
					else :
						dico2[ident+"_"+site]['classif']=0
	return(dico2)
				
	
					

if __name__ == "__main__":
	fichier_kinase = "Kinase_Substrate_Dataset"
	fichier_studies = "Phosphorylation_site_dataset"
	kinase = "Akt_kin"
	liste = "AF_list.csv"
	#positifs(fichier_kinase, fichier_studies, kinase, liste)
	#fetch_seqs(fichier_kinase, fichier_studies, kinase, liste)
	
	
	fichier_identifiant='Positives_Akt.tsv'
	fichier_phosphorylation='Phosphorylation_site_dataset'
	fichier_kinase="Kinase_Substrate_Dataset"
	kinase='Akt_kin' 
	stringence='Low' 


	d1, d2 = init_dico(fichier_identifiant,kinase,stringence)
	# ~ with open("dfinal_akt_low.txt",'r') as json_f:
		# ~ d1=json.load(json_f)

	# ~ with open ("dutile_akt_low.txt",'r') as json_f:
		# ~ d2=json.load(json_f)

	#d3 = score_conservation(d1,d2)
	d4, d5 = score_invivo(fichier_phosphorylation, fichier_kinase, d2, d1, kinase)
	pprint(d5)
	
	d6=comparaison_pos("comparaison_pos_akt.tsv", d5, d4)
	
	with open ("jeu_apprentissage_akt.tsv", "w") as output:
		print('identifiant','percentile','score_ss','score_conservation','score_invivo','classif', file=output, sep="\t")
		for key, value in d6.items():
			print(key, value['percentile'], value['score'], value['score_conservation'], value['score_invivo'], value['classif'], file = output, sep = "\t")
			
	pprint (d6)

