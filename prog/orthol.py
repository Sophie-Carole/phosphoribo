################################################################################
#
#Fonctionne dans un environnnement avec python 3.7 et python 2
#
#- Obtention séquences protéiques orthologues
#- Création fichiers fasta
#- Alignement multiple (mafft) et score de conservation par position (capra et singh 2007 (score_conservation.py))
#- Calcul d'un score pour le motif de phosphorylation
#
################################################################################



### Importation des packages
import requests 
from pprint import pprint
import re 
import sys
import pandas as pd # Pour stocker/ manipuler/ lire des données grace à des dataframe
from bs4 import BeautifulSoup # Pour l'analyse des textes/ fichiers HTML et XML 
import time 
from Bio import SeqIO, AlignIO # Pour la lecture & alignement des séquences
import os, ssl
import csv
from Bio.Align import _aligners
from scansite import *
from psp import*




"""

Recherche de proteines orthologues à une prot donnée (ID) dans une espèce donnée (specie)
Recherche dans la base de données Ensembl 

"""

def orthol(ID, specie):
    orthologues=[]
    server = "https://rest.ensembl.org"
    ext = "/homology/id/"+ID+"?type=orthologues;target_species="+specie;"type=orthologues"
    r=requests.get(server+ext, headers={ "Content-Type" : "text/x-orthoxml+xml"})

    if not r.ok:
        r.raise_for_status()
        sys.exit()
    doc=r.text

    soup=BeautifulSoup(doc, 'html.parser') 

    for el in soup.find_all('gene'):
        orthologues.append(el.get('protid'))
    return orthologues


"""

Récupération de la séquence proteique
Recherche dans la base de données Ensembl 

"""

def recup_sequence(ID):
    server = "https://rest.ensembl.org"
    ext = "/sequence/id/"+ID+"?type=protein"
    r = requests.get(server+ext, headers={ "Content-Type" : "text/x-fasta"}, params=[('q','requests+language:python')])
 
    if not r.ok: 
        r.raise_for_status()
        sys.exit()
    
    fasta=r.text
    return(fasta)

 
"""

Boucle sur les protéines du jeu de données et les espèces orthologues cibles
Ecriture des fichiers fasta dans le répertoire "fasta_files"

"""

def creation_fichiers_fasta (fichier):
	df=pd.read_csv(fichier, sep='\t')
	species=("danio_rerio","mus_musculus","rattus_norvegicus","caenorhabditis_elegans","gallus_gallus","drosophila_melanogaster")
	for row in df.itertuples():
		uniprotID=row[1]
		IdEnsembl=row[3]
		file="fasta_files/"+uniprotID+".fasta"
		with open(file, 'w') as f:
			for specie in species:
				orthologs=orthol(IdEnsembl, specie)
				if len(orthologs) != 0:
					ID_HumanProt=orthologs.pop(0)
					if len (orthologs) >= 2 :
						while len(orthologs) !=0 :
							ID_orthol=orthologs.pop()
							seq_fasta=recup_sequence(ID_orthol)
							f.write(seq_fasta)
					else:
						ID_orthol=orthologs.pop()
						seq_fasta=recup_sequence(ID_orthol)
						f.write(seq_fasta)


"""

Ajout du peptide encadrant le site phosphorylé prédit au fichier fasta
Alignement multiple (mafft)
Score de conservation (Capra & Singh)  

"""

def alignement(motif, site, fichier_fasta):
	file =fichier_fasta
	fR=open(file, 'r') 
	text=fR.read()
	fR.close
	
	fW=open(file.replace(".fasta", "_"+site+".fasta"),'w')
	fW.write(text+">motif\n"+motif)
	
	fichier_fastam=file.replace(".fasta", "_"+site+".fasta")
	fW.close()
	os.system("mafft --auto "+fichier_fastam+" > "+fichier_fastam.replace("fasta_files/","aligned_files/"))
	
	tmp=file.replace("fasta_files/", "")
	score=tmp.replace(".fasta","")
	
	os.system("python2 score_conservation.py -s js_divergence -w 3 -o ./score_conservation/"+score+"_"+site+".scores "+fichier_fastam.replace("fasta_files/","aligned_files/"))
	
	
	#os.remove(file.replace(".fasta", "_"+site+".fasta")) #suppression du fichier temporaire


"""

Ecriture des fichiers d'alignement pour tous les sites prédits dans le répertoire "aligned_files"
Ecriture des fichiers avec score dans le répertoire "score_conservation"

"""

def create_files(dictionnaire_utile):
	donnees_sites=dictionnaire_utile
	for ident,sites in donnees_sites.items():
		for site in sites['sites'].keys():
			motif=sites['sites'][site]['motif']
			print(motif)
			alignement(motif, site, "fasta_files/"+ident+".fasta") # création de fichiers d'alignements 
			

"""

Extraction des valeurs de score par position seulement pour l'alignement au niveau du peptide
Calcul score pour positions correspondants au motif de phosphorylation de la kinase considérée 

"""

def score_c(fichier_scores, kinase):  #par fichier
	liste=[]
	score=0
	df=pd.read_csv(fichier_scores, sep='\t', header=0, index_col=None, skiprows=1, names=('col_number','score','column'))
	
	motif={}
	i=0
	for row in df.itertuples():
		for element in row[3]:
			liste.append(element)
		dernier=liste.pop()
		if dernier != "-":
			i+=1
			motif[i]={'aa':'', 'no_col':'', 'score':''}
			motif[i]['aa']=dernier
			motif[i]['no_col']=row[1]
			motif[i]['score']=row[2]
			
	if kinase == "Akt_Kin":
		R1=motif[3].get("score")
		R2=motif[5].get("score")
		ST=motif[8].get("score")
		
		score= (R1+R2+ST)/3
	
	if kinase == "ErkDD":
		R1 = motif[1].get("score")
		R2 = motif[2].get("score")
		ST = motif[8].get("score")
		pos6 = motif[6].get("score")
		pos10 = motif[10].get("score")

		score = (R1+R2+ST+pos6+pos10)/5
		
	if kinase == "Erk1_Bind":
		F1 = motif[5].get("score")
		F2 = motif[7].get("score")
		P = motif[8].get("score")
		
		score = (F1+F2+P)/3
	
	if kinase == "Erk1_Kin":
		P = motif[9].get("score")
		ST = motif[8].get("score")
		
		score = (P+ST)/2

	return score


"""

Boucle pour chaque site prédits

"""	
	
def score_conservation(dico_final) : 
	for key, el in dico_final.items():
		kinase=el["kinase"]
		score_cons=score_c("score_conservation/"+key+".scores", kinase)
		el["score_conservation"]=score_cons
		
	return dico_final
	

if __name__ == "__main__":
	#creation_fichiers_fasta('identifiants.tsv')
	#IDs=["ENSG00000204348","ENSG00000206346","ENSG00000236765","ENSG00000228875","ENSG00000224398","ENSG00000230700","ENSG00000232960"]
	#for ID in IDs:
	#	print(orthol(ID,"rattus_norvegicus"))
	
	fichier_identifiant='identifiants_test.tsv'
	fichier_phosphorylation='Phosphorylation_site_dataset'
	fichier_kinase="Kinase_Substrate_Dataset"
	kinase='Akt_kin' 
	stringence='Low' 


	d1, d2 = init_dico(fichier_identifiant,kinase,stringence)
	
	pprint(d1)

	#d3 = score_conservation(d1,d2)
	d4, d5 = score_invivo(fichier_phosphorylation, fichier_kinase, d2, d1, kinase)
	pprint(d5)
	
	#create_files(d5)
	
	d6=score_conservation(d4)
	
	pprint(d6)

	#score("score_conservation/Q92747_S147.scores","Akt_kin")
