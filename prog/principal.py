### Importation des packages
import requests 
from pprint import pprint
import re #à utiliser dans extrac et à remplacer par beautifulsoup par la suite
import sys
import pandas as pd # Pour stocker/ manipuler/ lire des données grace à des dataframe
from bs4 import BeautifulSoup # Pour l'analyse des textes/ fichiers HTML et XML 
import time 
from Bio import SeqIO, AlignIO # Pour la lecture & alignement des séquences
#import os, ssl # ??
#from Bio.Align.applications import ClustalOmegaCommandline
import codecs #Pour sauvegarder tous les nouveaux fichiers en UTF8 = encodage 
import csv
import urllib.parse
import urllib.request
from scansite import *
from psp import*


fichier_identifiant='identifiants_test.tsv'
fichier_phosphorylation='Phosphorylation_site_dataset'
fichier_kinase="Kinase_Substrate_Dataset"
kinase='Akt_kin' #à rendre modififiable par l'utilisateur (argparse pour commencer ?) 
stringence='Low' #pareil
poids_LT=0.5
poids_MS=0.5
poids_invitro=0.5




d1, d2 = init_dico(fichier_identifiant,kinase,stringence)
#d3 = score_conservation(d1,d2)
d4, d5 = score_invivo(fichier_phosphorylation, fichier_kinase, poids_MS, poids_LT, poids_invitro, d2, d1, kinase)
#pprint(d4)
pprint(d5)
with open ("test.tsv", "w") as output: 
	print('identifiant','percentile','score_ss','score_conservation','score_invivo', file=output, sep="\t")
	for key, value in d4.items():
		print(key, value['percentile'], value['score'], value['score_conservation'], value['score_invivo'], file = output, sep = "\t")
