### Importation des packages
import requests 
from pprint import pprint
import re #à utiliser dans extrac et à remplacer par beautifulsoup par la suite
import sys
import pandas as pd # Pour stocker/ manipuler/ lire des données grace à des dataframe
import numpy as np
from bs4 import BeautifulSoup # Pour l'analyse des textes/ fichiers HTML et XML 
import time 
from Bio import SeqIO, AlignIO # Pour la lecture & alignement des séquences
#import os, ssl # ??
#from Bio.Align.applications import ClustalOmegaCommandline
import codecs #Pour sauvegarder tous les nouveaux fichiers en UTF8 = encodage 
import csv
import urllib.parse
import urllib.request
from scansite import *
import json




"""
PSP: Partie PhosphoSitePlus
"""


"""
Fichier: "Phosphorylation_site_dataset"
Pour une kinase donnée, la fonction cherche les sites qui ont été vus comme phosphorylés (+/- 1position) et
rapporte les données associées: LT_LIT,MS_LIT,MS_CST, etc...

Augmentation de la structure de données de stockage des infos sur les sites avec ces nouvelles données
La fonction retourne la structure de données augmentée. 
"""

def psp_phosphorylation_site_dataset(fichier_pd, dico_infos_sites): 
	df = pd.read_csv(fichier_pd, sep='\t')
	df.fillna(0.0,inplace=True)
	donnees_sites=dico_infos_sites
	for ident,sites in donnees_sites.items():
		for site in sites['sites'].keys():
			for row in df.itertuples():
				if row[3]==ident and row[7]=='human':
					site_phosph = row[5].replace("-p","")
					Residu_invivo= site_phosph[0]
					pos_invivo= int(''.join([str(i) for i in site_phosph if i.isdigit()])) #Pour extraire la position du résdu phosphorylé
					site_predit= site  
					Residu_predit= site_predit[0] #ResiduPhosphorylé dans Scansite pour meme prot
					pos_predit= int(''.join([str(i) for i in site_predit if i.isdigit()]))
					if (Residu_invivo == Residu_predit) and (pos_invivo == pos_predit or pos_invivo==pos_predit-1 or pos_invivo==pos_predit+1): #Tolérance d'erreur = 1 position
						LT_LIT= row[11]
						# ~ if row[11] is empty:
							# ~ LT_LIT=0.0
						MS_LIT= row[12]
						# ~ if row[12] is empty:
							# ~ MS_LIT=0.0
						MS_CST= row[13]
						# ~ print(ident, site_phosph,site_predit,LT_LIT,MS_LIT,MS_CST, row[10], sites['sites'][site]['motif'], sep=" -- ")
						sites['sites'][site]['LT_LIT']=float(LT_LIT)
						sites['sites'][site]['MS_LIT']=float(MS_LIT)
						sites['sites'][site]['MS_CST']=float(MS_CST)
	return donnees_sites
                # ~ # S477 et S478 / Y171 et Y170 / T146 et T147/ ..

                

"""
Fichier: "Kinase_substrate_Dataset"
*permet de récuperer si le site a été vu phosphorylé in vivo ou in vitro et par quelle kinase (nom de la kinase + Acc_ID)

Nous permet pour la kinase d'intéret de savoir si le site prédit a été vu phosphorylé in vitro ou in vivo. 
ces nouvelles informations viennent s'ajouter à notre structure de données qui contient les infos sur les sites 

La fonction retourne la structure de données augmentée
"""
        
def psp_kinase_substrate_dataset(fichier_kinase_dataset, kinase, dico_infos_sites): 
	df= pd.read_csv(fichier_kinase_dataset, sep='\t') 
	équivalences_kinases={"Akt_kin" : ("Akt1", "Akt2", "Akt3"), "mTOR": ("mTOR"), "ErkDD": ("ERK1", "ERK2", "ERK3")}  #à compléter 
	donnees_sites=dico_infos_sites
	for kin in équivalences_kinases[kinase]:
		for ident,sites in donnees_sites.items():
			for site in sites['sites'].keys():
				for row in df.itertuples():
					if row[7]==ident and row[4]=='human' and row[2]==kin:
						site_phospho=row[10]
						Residu_invivo= site_phospho[0]
						pos_invivo= int(''.join([str(i) for i in site_phospho if i.isdigit()]))
						site_predit= site 
						Residu_predit= site_predit[0] #ResiduPhosphorylé dans Scansite pour meme prot
						pos_predit= int(''.join([str(i) for i in site_predit if i.isdigit()]))
						if (Residu_invivo == Residu_predit) and (pos_invivo == pos_predit or pos_invivo==pos_predit-1 or pos_invivo==pos_predit+1):
							if row[15]== 'X':
								evidence_in_vitro=row[15]
								sites['sites'][site]['in_vitro']=0.0
							if row[14]=='X':
								evidence_in_vivo=row[14]
								sites['sites'][site]['in_vivo']=1.0
								
							#print(site_phospho, site_predit, row[1], row[2], sep=" -- ")
	return donnees_sites
					




"""
Fichier: "Disease_Associated_Sites"
Pour une kinase donnée, la fonction permet de chercher les maladies associées aux sites de phosphorylation prédits par ScanSite 
& crée un fichier avec les données croisées de l'analyse des 2 tables / fichiers
"""


def psp_das(fichier_sc, fichier_das, fichier_sortie):
    with codecs.open(fichier_sortie, 'a','utf-8') as fout:
        writer = csv.writer(fout,  delimiter = '\t', lineterminator='\n')
        entetes = ['Entry_name','UniProt_ID', 'Site_SC', 'Kinase_SC', 'Motif', 'Percentile', 'Score',"site_PSP", "Alteration", "Maladie", "Proteine-domaine", "lt_lit", "ms_lit", "ms_cst", "Notes"]
        # Headers:
        ligneEntete = "\t".join(entetes) + "\n"
        fout.write(ligneEntete)    

        dfa = pd.read_csv(fichier_das, sep=';', encoding= 'utf8')
        dfb= pd.read_csv(fichier_sc, sep=';', encoding= 'utf8')

        for linea in dfa.itertuples():
            #Iere verification:
            if linea[10]== 'human': # On ne COMMENCE PAS de 0 mais de 1
                ligne=[]
                for lineb in dfb.itertuples():
                    # IIeme verification:
                    if linea[6]== lineb[2]: # Si c'est le meme gène / prot
                        if (linea[12]).endswith('-p'): #verifier qu'il s'agit d'une phosphorylation (et pas méthylation/ acétylation..)
                            site_phosph = linea[12].replace("-p","")
                            lettre1= site_phosph[0]
                            num= int(''.join([str(i) for i in site_phosph if i.isdigit()]))
                            site_sc= lineb[3]
                            lettre2= site_sc[0]
                            numsc= int(''.join([str(i) for i in site_sc if i.isdigit()]))

                            if (lettre1 == lettre2) and (num == numsc or num==numsc-1 or num==numsc+1):
                                alt= linea[2]
                                dis= linea[1]
                                protdom= str(linea[4])+ " - "+ str(linea[13])
                                lt_lit= linea[16]
                                ms_lit= linea[17]
                                ms_cst= linea[18]
                                notes= linea[20]
                                ligne= [lineb[1],lineb[2],lineb[3],lineb[4],lineb[5],lineb[6], lineb[7], site_phosph, alt, dis, protdom, lt_lit, ms_lit, ms_cst, notes]
                                print(ligne)
                                writer.writerow(ligne)
        fout.close()



def score_invivo(fichier_phosphorylation, fichier_kinase, dictionnaire_utile, dictionnaire_final, kinase):
	donnees_sites=psp_phosphorylation_site_dataset(fichier_phosphorylation, dictionnaire_utile)
	donnees_sites=psp_kinase_substrate_dataset(fichier_kinase, kinase, donnees_sites)
	score=0
	for ident,sites in donnees_sites.items():
		for site in sites['sites'].keys():
			if sites['sites'][site]['MS_LIT'] == 0:
				scoreMS = 0
			else:
				scoreMS=np.log(sites['sites'][site]['MS_LIT'])*0.2  # valeur de score jusqu'à 0.8 (-1/x+1)*0.8
			if scoreMS > 0.8:
				scoreMS = 0.8
			if sites['sites'][site]['LT_LIT'] == 0:
				scoreLT = 0
			else:
				scoreLT=(-1/sites['sites'][site]['LT_LIT']+1)*0.2+0.8 # ! calcul non définitif, les valeurs de MS_LIT et LT_LIT pour le moment sont brutes et très variables (ne donne pas de valeur entre 0 et 1)
			#print(scoreMS)
			#print(scoreLT)
			if scoreLT > 0 :
				score = scoreLT
			else :
				score = scoreMS
			# ~ if sites['sites'][site]['in_vitro'] == "oui":
				# ~ score =poids_invitro* score
			if sites['sites'][site]['in_vivo'] == "oui":
				score = 1
			dictionnaire_final[ident+"_"+site]['score_invivo']=score
	return dictionnaire_final, donnees_sites



if __name__ == "__main__":
	file='Phosphorylation_site_dataset'
	d1, d2 = init_dico('identifiants_test.tsv','Akt_kin','Low')
    # ~ pprint(d1)
	# ~ pprint(d2)
	#pprint(psp_phosphorylation_site_dataset(file,d2))
	
	"""
    ex de résultat, dictionnaire de stockage augmenté avec les infos concernant :
    
    
     'Q9NY61': {'sites': {'S143': {'LI_LIT': 1.0,
                               'MS_CST': 1.0,
                               'MS_LIT': nan,
                               'kinase': 'Akt_Kin',
                               'motif': 'SKKSRSHsAKTPGFS',
                               'percentile': '0.010535995508527261',
                               'score': '0.5828'},
                      'S478': {'LI_LIT': 1.0,
                               'MS_CST': nan,
                               'MS_LIT': nan,
                               'kinase': 'Akt_Kin',
                               'motif': 'LIERKTSsLDPNDQV',
                               'percentile': '0.027097141218783035',
                               'score': '0.6618'},
                      'T366': {'LI_LIT': 1.0,
                               'MS_CST': nan,
                               'MS_LIT': 1.0,
                               'kinase': 'Akt_Kin',
                               'motif': 'FTVYRNRtLQKWHDK',
                               'percentile': '0.041536002983468615',
                               'score': '0.7021'}},
            'uniprotID': 'AATF_HUMAN'},
	'Q9P287': {'sites': {}, 'uniprotID': 'BCCIP_HUMAN'},
	'Q9UG63': {'sites': {}, 'uniprotID': 'ABCF2_HUMAN'},
	'Q9ULW3': {'sites': {'S136': {'LI_LIT': nan,
                               'MS_CST': 1.0,
                               'MS_LIT': nan,
                               'kinase': 'Akt_Kin',
                               'motif': 'PMGARRRsPFRYDLW',
                               'percentile': '0.04058894830888906',
                               'score': '0.7'},
                      'T102': {'kinase': 'Akt_Kin',
                               'motif': 'GGKKRSYtKDYTEGW',
                               'percentile': '0.004530879394782013',
                               'score': '0.5226'},
                      'T238': {'kinase': 'Akt_Kin',
                               'motif': 'RERARLAtAQDKARS',
                               'percentile': '0.0031394106644534805',
                               'score': '0.4974'}},
            'uniprotID': 'ABT1_HUMAN'}}

	"""
    
	psp_kinase_substrate_dataset("Kinase_Substrate_Dataset","Akt_kin",d2)
