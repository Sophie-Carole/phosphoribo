## scansite.py 

Fait la recherche sur scansite (API)

Possibilité de préciser la kinase recherchée et la stringence de la recherche

Retourne un dictionnaire où la clé principale est l'association entre l'identifiant de la protéine et le site prédit: 

```{r}
 'Q9H583_I830': {'kinase': 'ErkDD',
                 'percentile': '0.024050562819329466',
                 'score': '0.685',
                 'score_conservation': '0',
                 'score_invivo': '0',
                 'site': 'I830'},
```

| identifiant | site | kinase | percentile | score_ss | score_conservation | score_invivo 
| ------ | ------ | ------ | ------ | ------ | ------ | ------ 
| Q9H583_I830 | I830 | ErkDD | 0.024050562819329466 | 0.685 | 0 | 0 | 
| ... | ... | ... | ... | ... | ... | ... | 

#### cas particulier de recherche de ERK:

Le dictionnaire contient les prédictions pour ERK D Domain et ERK1.

Les prédiction pour ERK1 sont inclus dans le dictionnaire ssi ERK1 binding site a également été prédit sur la protéine à une distance pour le moment de 100 aa (décision arbitraire)

```{r}
 'Q9H583_S344': {'kinase': 'Erk1_Kin',
                 'percentile': '0.04530431821685042',
                 'score': '0.6489',
                 'score_conservation': '0',
                 'score_invivo': '0',
                 'site': 'S344'},
 'Q9H583_V1072': {'kinase': 'ErkDD',
                  'percentile': '0.0369930122375773',
                  'score': '0.7241',
                  'score_conservation': '0',
                  'score_invivo': '0',
                  'site': 'V1072'},
```

## psp.py 

Effectue recherche dans tables disponibles sur PhosphoSitePlus

Recherche le site prédit pour une protéine donnée à 1 position près (prise en compte des cas où deux S ou deux T côte à côte qui ont causé une erreur dans la prédiction)

- (1) Dans la table phosphorylation site dataset

Récupération du nombre d'occurence dans la littération : low throughput (LT) et mass spectrometry (MS)

- (2) Dans la table kinase substrate dataset 

Pour la kinase d'intéret, vérifie si site prédit présent dans la table et information si données provient de : in vitro ou in vivo

- (3) Calcul d'un score 

Pour le moment:

poidsLT * LT + poidsMS * MS  (encore à améliorer) 

Si de (2), il y a cas in vivo alors score maximal attribué (pour le moment = 1)


## orthol.py

Recherche orthologues pour la protéine donnée sur la base de données ensembl (uniquement vertébrés) et en récupère la séquence

Liste d'organimes = danio_rerio, mus_musculus, rattus_norvegicus, caenorhabditis_elegans, gallus_gallus, drosophila_melanogaster

- création de fichier fasta pour chaque protéines

- ajout du motif à aligner au fichier fasta puis alignement mutiple avec mafft

- pas encore réalisé : calcul d'un score de conservation

avec soit ConSurf, méthode basée sur evolutionnary rate, soit le programme de Capra & Singh (Bioinformatics 2007) qui utilise Jensen-Shannon divergence

Dans les deux cas: score calculé pour chaque résidus du motif

Score de conservation serait médiane (ou moyenne?) des scores des résidus


